FROM python:alpine
RUN pip install snapcastr
EXPOSE 5000
CMD ["/bin/sh", "-c", "snapcastrd", "--port 5000", "--host=0.0.0.0","--sc_host $SNAPCAST_HOST"]
